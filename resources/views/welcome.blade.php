<!DOCTYPE html>
<html lang="hu">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Iskolai Étkezés megrendelő</title>
    <meta content="Iskolai étkezés megrendelő" name="description">
    <meta content="Ebédbefizetés" name="keywords">

    <!-- Boostrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <!-- Icons-->
    <link href="{{asset('/assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">

    <!-- CSS File -->
    <link href="{{asset('/assets/css/style.css')}}" rel="stylesheet">

</head>

<body>

<!-- ======= Header ======= -->
<header id="header">
    <div class="container-fluid">

        <div class="logo">
            <h1 style="color: white">Iskolai Étkezés megrendelő</h1>
        </div>

        <button type="button" class="nav-toggle"><i class="bx bx-menu"></i></button>
        <nav class="nav-menu">
            <ul>
                <li class="active"><a href="#header" class="scrollto">Főoldal</a></li>
                <li><a href="#about" class="scrollto">Rólunk</a></li>
                <li><a href="#why-us" class="scrollto">Miért minket kell választani?</a></li>
            </ul>
            <div class="nav-divider"></div>
            <ul class="navbar-nav ms-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="scrollto" href="{{ route('login') }}">{{ __('Bejelentkezés') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="scrollto" href="{{ route('register') }}">{{ __('Regisztráció') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </nav>
    </div>
</header><!-- End header -->

<!-- ======= Hero Section ======= -->
<section id="hero">
    <div class="hero-container">
        <h1>Üdvözöljük Weboldalunkon</h1>
    </div>
</section><!-- hero -->

<main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <img src="{{asset('assets/img/menza.jpg')}}" class="img-fluid" alt="">
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0">
                    <h3>Rendelje meg gyereke étkezését minden eddiginél gyorsabban</h3>
                    <ul>
                        <li><i class="bx bx-check-double"></i> Étkezés megrendelése otthonról vagy akár az iskolában</li>
                        <li><i class="bx bx-check-double"></i> Bankártyás és Kézpénzes fizetés</li>
                        <li><i class="bx bx-check-double"></i> 1 ,2 vagy 3 étkezés választható</li>
                        <li><i class="bx bx-check-double"></i> A heti Étlap megtekintése</li>
                    </ul>
                    <p>
                        Ez a weboldal lehetővé teszi azt ,hogy ön anélkül ,hogy esetleg gyerekével kéne pénzt beküldeni amit elhagyhat,
                        e weboldal segítségével othhonról ,vagy előre egyeztetett időpontban pillanatok alatt elintézze ezt az általában
                        bonyolult folyamatot.
                    </p>
                </div>
            </div>

        </div>
    </section><!-- End About Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
        <div class="container">

            <div class="row">
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <img src="{{asset('assets/img/etterem.jpg')}}" class="card-img-top" alt="..." style="height: 220px">
                        <div class="card-icon">
                            <i class="bx bx-book-reader"></i>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="">Célunk</a></h5>
                            <p class="card-text">Minnél több gyerek vegyen részt az iskolai étkezésben és szokjon az egézségesebb ételek irányába. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <img src="{{asset('assets/img/chef.jpg')}}" class="card-img-top" alt="..." style="height: 220px">
                        <div class="card-icon">
                            <i class="bx bx-calendar-edit"></i>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="">A tervünk</a></h5>
                            <p class="card-text">Minnél több munkalehetőség olyan konyhában dolgozoknak akiknek nem jutott máshol hely és szeretik a gyerekeket. </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 d-flex align-items-stretch">
                    <div class="card">
                        <img src="{{asset('assets/img/szulo.jpg')}}" class="card-img-top" alt="..." style="height: 220px">
                        <div class="card-icon">
                            <i class="bx bx-landscape"></i>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><a href="">Miért jobb mint eddig?</a></h5>
                            <p class="card-text">Ez a weboldal egy olyan terhet vesz le a szülőkről mint például a Minden napos főzés , kisseb gyerekkel pénzeket küldeni és a sttresszelés hogy evett e már ma a gyerek. </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section><!-- End Why Us -->


</main><!-- End main -->

<!-- Javascript -->
<script src="{{asset('/assets/js/main.js')}}"></script>

</body>

</html>
