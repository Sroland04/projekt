@extends('layouts.parent')




@section('content')

    <div class="container">

<h1 class="text-center">Saját Adatok</h1>


        <div class="mb-3">
            <label for="name" class="form-label">Saját Név</label>
            <input type="text" class="form-control" id="name" name="name" readonly value="{{\Illuminate\Support\Facades\Auth::user()->name}}">
        </div>


        <div class="mb-3">
            <label for="name" class="form-label">Saját Email-cím</label>
            <input type="email" class="form-control" id="email" name="email" readonly value="{{\Illuminate\Support\Facades\Auth::user()->email}}">
        </div>

        <div class="mb-3">
            <label for="name" class="form-label">Saját Telefonszám</label>
            <input type="text" class="form-control" id="phone" name="phone" readonly value="{{\Illuminate\Support\Facades\Auth::user()->phone}}">
        </div>

        <div class="mb-3">
            <label for="name" class="form-label">Saját Születési Dátum</label>
            <input type="date" class="form-control" id="birth" name="birth" readonly value="{{\Illuminate\Support\Facades\Auth::user()->birth}}">
        </div>



    </div>



@endsection
