@extends('layouts.parent')




@section('content')

<div class="container">
    <h1 style="text-align: center">Gyermekeim Adatai</h1>
</div>

    <div class="container">
        <div class="row">
        @foreach($children as $child)
            <div class="col-lg-6" style="margin-top: 3%">
                 <div class="card" style="margin-top: 5px;">
                     <div class="card-body">
                <div class="mb-3">
                    <h2 for="name" class="form-label">{{$child->name}}</h2>
                </div>


                <div class="mb-3">
                    <label for="name" class="form-label">Gyermekem Születési dátuma</label>
                    <input type="date" class="form-control" id="birth" name="birth" value="{{$child->birth}}" readonly/>
                </div>


                <div class="mb-3">
                    <label for="name" class="form-label">Gyermekem életkora</label>
                    <input type="text" class="form-control" id="age" name="age" value="{{$child->age}}" readonly/>
                </div>


                <div class="mb-3">
                    <label for="name" class="form-label">Gyermekem étkeezési azonosítója</label>
                    <input type="text" class="form-control" id="Omnumber" name="Omnumber" value="{{$child->Omnumber}}" readonly/>
                </div>
            </div>
        </div>
        </div>

        @endforeach
        </div>
    </div>











@endsection
