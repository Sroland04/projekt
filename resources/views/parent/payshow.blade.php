@extends('layouts.parent')






@section('content')

    <div class="container">
        <h1 class="text-center">Ebéd befizetése</h1>

        @if (Session::has("success"))

            <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    {{ session()->get('success') }}
                </strong>
            </div>

        @elseif(Session::has("error"))
            <div class="alert alert-dismissable alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    {!! session()->get('error') !!}
                </strong>
            </div>
        @endif



        @error('child')
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="text-danger">{{$message}}</strong>
        </div>
        @enderror


        @error('until')
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="text-danger">{{$message}}</strong>
        </div>
        @enderror


        <form action="{{route('parentpay')}}" method="POST">
            @csrf
            @method('POST')

            <div class="mb-3">
                <label for="name" class="form-label">Gyermek(-ek) Kiválasztása</label>
                <select class="form-control" aria-label="Default select example" name="name" id="name">
                    @foreach($children as $child)
                    <option value="{{$child->name}}">{{$child->name}}</option>
                    @endforeach
                </select>
            </div>


        <div class="mb-3">
            <label for="name" class="form-label">Befizetés Vége</label>
            <input type="date" class="form-control" id="until" name="until" value="{{old('until')}}"/>
        </div>






        <div class="text-center">
            <button type="submit" name="submit" id="submit" class="btn btn-primary">Befizetés</button>
        </div>
        </form>
    </div>









@endsection
