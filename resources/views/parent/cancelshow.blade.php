@extends('layouts.parent')




@section('content')


<div class="container" >
    <h1 class="text-center" style="margin-top: 1px">Ebéd lemondása</h1>

    @if (Session::has("success"))

        <div class="alert alert-dismissable alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>
                {{ session()->get('success') }}
            </strong>
        </div>

    @elseif(Session::has("error"))
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>
                {!! session()->get('error') !!}
            </strong>
        </div>
    @endif

            <form action="{{route('parentcancel')}}" method="POST">
                @csrf
                @method('POST')

                <div class="mb-3">
                    <label for="name" class="form-label">Gyermek(-ek) Kiválasztása</label>
                    <select class="form-control" aria-label="Default select example" name="name" id="name">
                        @foreach($children as $child)
                            <option value="{{$child->name}}">{{$child->name}}</option>
                        @endforeach
                    </select>
                </div>
                @foreach($children as $child)
                <div class="mb-3">
                    <label for="name" class="form-label">{{$child->name}} Eddig befizetve:</label>
                    <input type="date" class="form-control" id="until" name="until" readonly value="{{$child->until}}"/>
                </div>
                @endforeach

                <div class="text-center">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Lemondás
                </button>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Biztosan lemondja az étkezést?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Ha lemondja az étkezést újra kell fizetnie a kívánt időszakot!</p>
        </div>




    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégse</button>
        <button type="submit" name="submit" id="submit" class="btn btn-primary">Lemondás</button>
    </div>
        </div>
             </div>
                    </div>
                         </form>





@endsection
