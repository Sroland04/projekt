@extends('layouts.admin')

@section('content')









    <!-- Begin Page Content -->

            <div class="container-fluid">

                <!-- Page Heading -->
                <h1  style="text-align: center">Felhasználók</h1>

                @if (Session::has("success"))

                        <div class="alert alert-dismissable alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>
                                {{ session()->get('success') }}
                            </strong>
                        </div>

                @elseif(Session::has("error"))
                    <div class="alert alert-dismissable alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>
                            {!! session()->get('error') !!}
                        </strong>
                    </div>
                @endif



                @error('name')
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <strong class="text-danger">{{$message}}</strong>
                </div>
                @enderror


                @error('email')
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="text-danger">{{$message}}</strong>
                </div>
                @enderror




                @error('is_admin')
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="text-danger">{{$message}}</strong>
                </div>
                @enderror


                @error('phone')
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="text-danger">{{$message}}</strong>
                </div>
                @enderror

                @error('birth')
                <div class="alert alert-dismissable alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong class="text-danger">{{$message}}</strong>
                </div>
                @enderror



                <!-- DataTales Example -->
                <input type="text" id="myInput"onkeyup="myFunction()" class="form-control" style="width: 400px;margin-bottom: 20px" placeholder="Név keresése...">
                <div class="card shadow mb-4">

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="myTable"  width="100%" cellspacing="0">

                                <thead>

                                <tr>
                                    <th>Felhasználó azonosítója</th>
                                    <th>Felhasználó neve</th>
                                    <th>Felhasználó email-címe</th>
                                    <th>Felhasználó telefonszáma</th>
                                    <th>Felhasználó születési dátuma</th>
                                    <th>Felhasználó Rangja</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $data)
                                <tr>
                                    <td>{{$data->id}}</td>
                                    <td>{{$data->name}}</td>
                                    <td>{{$data->email}}</td>
                                    <td>{{$data->phone}}</td>
                                    <td>{{$data->birth}}</td>
                                    <td>{{$data->is_admin}}</td>
                                </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>

            </div>



    <script>
        function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>

@endsection

