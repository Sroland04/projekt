@extends('layouts.admin')

@section('content')




    <!-- Begin Page Content -->

    <div class="container-fluid">

        <!-- Page Heading -->
        <h1  style="text-align: center">Felhasználók Adatainak Módosítása</h1>






        <input type="text" id="myInput"onkeyup="myFunction()" class="form-control" style="width: 400px;margin-bottom: 20px" placeholder="Név keresése...">

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">

                        <thead>

                        <tr>
                            <th>Felhasználó azonosítója</th>
                            <th>Felhasználó neve</th>
                            <th>Felhasználói Adatok módósítása</th>

                        </tr>
                        </thead>

                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>
                                 <form action="{{route('adminshow')}}" method="POST">
                                     @method('POST')
                                     @csrf
                                     <input type="hidden" name="id"  value="{{$user->id}}">
                                     <input type="hidden" name="name" value="{{$user->name}}">
                                     <input type="hidden" name="phone" value="{{$user->phone}}">
                                     <input type="hidden" name="birth" value="{{$user->birth}}">
                                     <input type="hidden" name="email" value="{{$user->email}}">
                                     <input type="hidden" name="is_admin" value="{{$user->is_admin}}">
                                     <input type="hidden" name="password" value="{{$user->password}}">
                                        <button class="btn btn-warning" type="submit" name="submit" id="submit">Szerkesztés</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>

                    </table>
                </div>
            </div>
        </div>

    </div>

    <script>
        function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
@endsection
