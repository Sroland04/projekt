@extends('layouts.admin')

@section('content')










            <form action="{{route('admined')}}" method="POST">
                @method('POST')
                @csrf
            <div class="container">


                <h1 class="text-center">{{$user->name}} Adatainak Módosítása</h1>
                <input type="hidden" name="id" value="{{old('id',$user->id)}}">








                <div class="mb-3">
        <label for="name" class="form-label">Felhasználó Neve</label>
        <input type="text" class="form-control" id="name" name="name" value="{{old('name',$user->name)}}">
    </div>



    <div class="mb-3">
        <label for="email" class="form-label">Felhasználó Email-címe</label>
        <input type="email" class="form-control" id="email" name="email" value="{{old('email',$user->email)}}">
    </div>


    <div class="mb-3">
        <label for="is_admin" class="form-label">Felhasználó Rangja</label>
        <select class="form-control" aria-label="Default select example" name="is_admin" id="is_admin">
            @if($user->is_admin=="Szülő")
                <option selected value="Szülő">Szülő</option>
                <option value="Admin">Admin</option>
            @else
                <option selected value="Admin">Admin</option>
                <option value="Szülő">Szülő</option>

            @endif
        </select>
    </div>



    <div class="mb-3">
        <label for="phone" class="form-label">Felhasználó Telefonszáma</label>
        <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone',$user->phone)}}">
    </div>


    <div class="mb-3">
        <label for="birth" class="form-label">Felhasználó Születési Dátuma</label>
        <input type="date" class="form-control" id="birth" name="birth" value="{{old('birth',$user->birth)}}">
    </div>


                <div class="mb-3">
                    <input type="hidden" class="form-control" id="password" name="password"  value="{{old('password',$user->password)}}">
                </div>




                <div class="text-center">
                    <button type="submit" name="submit" id="submit" class="btn btn-primary">Adatok frissitése</button>
                </div>


            </div>

            </form>

@endsection
