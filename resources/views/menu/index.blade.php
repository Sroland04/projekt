@extends('layouts.admin')





@section('content')
    <div class="container">
        <h1 style="text-align: center">Étlapok</h1>



        @if (Session::has("success"))

            <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    {{ session()->get('success') }}
                </strong>
            </div>

        @elseif(Session::has("error"))
            <div class="alert alert-dismissable alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    {!! session()->get('error') !!}
                </strong>
            </div>
        @endif


        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable">
            <thead>
            <tr>
                <th scope="col">Étlap Id</th>
                <th scope="col">Étlapok</th>
                <th scope="col">Étlap feltöltése</th>
                <th scope="col">Étlap Törlése</th>
            </tr>
            </thead>
            <tbody>
            @foreach($menus as $menu)
                <tr>
                    <td>{{$menu->id}}</td>
                    <td>
                        <img src="{{asset('storage/app/public/img/'.$menu->image) }}" alt="{{$menu->image}}"
                             style="height: 100px; width: 150px;">
                    </td>
                    <td>
                        <form action="{{route('menupload',$menu->id)}}" method="POST">
                            @csrf
                            @method('POST')
                            <input type="submit" class="btn btn-warning" value="Feltöltés" />
                        </form>
                    </td>
                    <td>
                        <form action="{{route('menudestroy',$menu->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger" value="Törlés" />
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
                </div>
            </div>
        </div>
    </div>




@endsection
