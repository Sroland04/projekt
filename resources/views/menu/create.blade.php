@extends('layouts.admin')



@section('content')





    <form method="post" action="{{ route('menustore') }}"
          enctype="multipart/form-data">
        @csrf
    <div class="container">
<div class="mb-3">
                <h1 style="text-align: center">Étlap feltöltése</h1>
    @if (Session::has("success"))

        <div class="alert alert-dismissable alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>
                {{ session()->get('success') }}
            </strong>
        </div>

    @elseif(Session::has("error"))
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>
                {!! session()->get('error') !!}
            </strong>
        </div>
    @endif


    @error('image')
    <div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong class="text-danger">{{$message}}</strong>
    </div>
    @enderror












    <div class="mb-3">
                <input type="file" class="form-control" required name="image" value="{{old('image')}}">
        </div>
            <div class="mb-3" style="text-align: center">
                <button type="submit" class="btn btn-primary">Feltöltés</button>
            </div>

</div>
</div>
    </form>

@endsection
