@extends('layouts.admin')




@section('content')

    <div class="container-fluid">

        <h1  style="text-align: center">Gyerekek</h1>

        @if (Session::has("success"))

            <div class="alert alert-dismissable alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    {{ session()->get('success') }}
                </strong>
            </div>

        @elseif(Session::has("error"))
            <div class="alert alert-dismissable alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    {!! session()->get('error') !!}
                </strong>
            </div>
        @endif



        @error('name')
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="text-danger">{{$message}}</strong>
        </div>
        @enderror


        @error('birth')
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="text-danger">{{$message}}</strong>
        </div>
        @enderror




        @error('age')
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="text-danger">{{$message}}</strong>
        </div>
        @enderror


        @error('Omnumber')
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="text-danger">{{$message}}</strong>
        </div>
        @enderror


        @error('parentId')
        <div class="alert alert-dismissable alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong class="text-danger">{{$message}}</strong>
        </div>
        @enderror



        <input type="text" id="myInput"onkeyup="myFunction()" class="form-control" style="width: 400px;margin-bottom: 20px" placeholder="Név keresése...">
        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">

                        <thead>

                        <tr>
                            <th>Gyerek azonosítója</th>
                            <th>Gyerek neve</th>
                            <th>Gyerek Születési dátuma</th>
                            <th>Gyerek életkora</th>
                            <th>Gyerek Om-azonosítója</th>
                            <th>Befizetve?</th>
                            <th>Eddig befizetve</th>
                            <th>Szülő Neve</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $data)
                            <tr>
                                <td>{{$data->id}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->birth}}</td>
                                <td>{{$data->age}}</td>
                                <td>{{$data->Omnumber}}</td>
                                @if($data->payed == 1)
                                <td>Igen</td>
                                @else
                                    <td>Nem</td>

                                @endif
                                <td>{{$data->until}}</td>
                                <td>{{$data->szulonev}}</td>
                            </tr>
                            @endforeach
                            </tbody>

                    </table>
                </div>
            </div>
        </div>

    </div>

    <script>
        function myFunction() {

            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");


            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>

@endsection
