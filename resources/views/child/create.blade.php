@extends('layouts.admin')



@section('content')

    <h1 class="text-center">Gyerek hozzáadása</h1>
    <div class="container">

        <form action="{{route('childstore')}}" method="POST">
            @csrf
    <div class="mb-3">
        <label for="name" class="form-label">Gyermek Neve</label>
        <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
    </div>


        <div class="mb-3">
            <label for="birth" class="form-label">Gyermek Születési dátuma</label>
            <input type="date" class="form-control" id="birth" name="birth" value="{{old('birth')}}">
        </div>


        <div class="mb-3">
            <label for="age" class="form-label">Gyermek Életkora</label>
            <input type="number" class="form-control" id="age" name="age" value="{{old('age')}}">
        </div>

        <div class="mb-3">
            <label for="om" class="form-label">Gyermek Étkezés azonosító</label>
            <input type="number" class="form-control" id="Omnumber" name="Omnumber" value="{{old('Omnumber')}}">
        </div>


            <div class="mb-3">
                <label for="om" class="form-label">Szülő-Id</label>
                <input type="number" class="form-control" id="parentId" name="parentId" value="{{old('parentId')}}">
            </div>

<div style="text-align: center">
    <button class="btn btn-primary" type="submit" name="submit" id="submit">Hozzáadás</button>
</div>

        </form>
    </div>

@endsection
