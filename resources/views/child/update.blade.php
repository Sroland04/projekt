@extends('layouts.admin')



@section('content')

    <h1 class="text-center">{{$child->name}} Adatainak Módosítása</h1>

    <form action="{{route('childup',$child)}}" method="POST">
        @method('POST')
        @csrf
        <div class="container">


            <div class="mb-3">
                <label for="name" class="form-label">Gyerek Neve</label>
                <input type="text" class="form-control" id="name" name="name" value="{{old('name',$child->name)}}">
            </div>


            <div class="mb-3">
                <label for="name" class="form-label">Gyerek születési dátuma</label>
                <input type="date" class="form-control" id="birth" name="birth" readonly value="{{old('birth',$child->birth)}}">
            </div>


            <div class="mb-3">
                <label for="name" class="form-label">Gyerek életkora</label>
                <input type="text" class="form-control" id="age" name="age" value="{{old('age',$child->age)}}">
            </div>

            <div class="mb-3">
                <label for="name" class="form-label">Gyerek Étkezési azonosítója</label>
                <input type="number" class="form-control" id="Omnumber" name="Omnumber" value="{{old('Omnumber',$child->Omnumber)}}">
            </div>


            <div class="text-center">
                <button type="submit" name="submit" id="submit" class="btn btn-primary">Adatok Frissitése</button>
            </div>


        </div>

    </form>



@endsection
