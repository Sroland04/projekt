@extends('layouts.admin')

@section('content')


    <div class="container-fluid">

        <h1  style="text-align: center">Gyerekek Adatainak módosítása</h1>
        <input type="text" id="myInput" onkeyup="myFunction()" class="form-control" style="width: 400px;margin-bottom: 20px" placeholder="Név keresése...">
        <div class="card shadow mb-4">

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">

                        <thead>

                        <tr>
                            <th>Gyerek azonosítója</th>
                            <th>Gyerek neve</th>
                            <th>Gyerek Adatok módósítása</th>

                        </tr>
                        </thead>

                        <tbody>
                        @foreach($children as $child)
                            <tr>
                                <td>{{$child->id}}</td>
                                <td>{{$child->name}}</td>
                                <td>
                                    <form action="{{route('childshow')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$child->id}}">
                                        <input type="hidden" name="name" value="{{$child->name}}">
                                        <input type="hidden" name="birth" value="{{$child->birth}}">
                                        <input type="hidden" name="age" value="{{$child->age}}">
                                        <input type="hidden" name="Omnumber" value="{{$child->Omnumber}}">
                                        <button class="btn btn-warning" type="submit" name="submit" id="submit">Szerkesztés</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

    </div>

    <script>
        function myFunction() {
            // Declare variables
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>
@endsection
