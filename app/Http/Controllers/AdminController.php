<?php

namespace App\Http\Controllers;

use App\Models\Child;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Console\View\Components\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Validator;
use MongoDB\Driver\Session;

class AdminController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $data = User::all();

        return view('admin.index',['data' => $data]);
    }


    public function create(){
        return view('admin.create');
    }

    public function edit(){

        $users = User::all();
        return view('admin.update',['users'=>$users]);


    }

    public function update(Request $request,User $user){


        $validate=\Illuminate\Support\Facades\Validator::make($request->all(),[
            'name'=>'required|regex:/^[\pL\s\-]+$/u|max:255',
            'email'=>'required|email',
            'is_admin'=>'required',
            'phone'=>'required|integer|digits:9',
            'birth'=>'required|date',
        ],[
            'name.required'=>'Kérjük add meg a felhasználó nevét',
            'name.regex'=>'A névben csak betük szerepelhetnek',
            'name.max'=>'Maximum 255 karakter',
            'email.required'=>'Kérjük add meg az email-címet',
            'email.email'=>'Kérjük figyelj az formátumra',
            'is_admin.required'=>'Kérjük add meg a felhasználó dátumát',
            'phone.required'=>'Kérjük add meg a telefonszámot',
            'phone.integer'=>'A telefonszám csak számokból állhat',
            'phone.digits'=>'A telefonszám csak 9 számból állhat',
            'birth.required'=>'Kérjük add meg a felhasználó születési dátumát',
            'birth.date'=>'Kérjük figyelj a születési dátum formátumára'
        ]);







        $user=User::find($request->id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->is_admin = $request->is_admin;
        $user->phone = $request->phone;
        $user->birth = $request->birth;



        if($validate->fails()) {
            return redirect('admin.index')->withErrors($validate->errors())->withInput();
        }
        else{
            $user->update();
            return redirect('admin.index',)->with('success','Az adatok frissitése sikeres volt');
        }





    }

    public function show(User $user, Request $request){


        $request->request->remove('_token');
        $request->request->remove('submit');



        return view('admin.edit',['user'=>$request]);
    }

    public function delete(){
        $data=User::all();
        return view('admin.delete',['data'=>$data]);
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();
        $user->update();
        return redirect('admin.delete')->with('success','A felhasználó törlése sikeresen megtörtént');
    }

}
