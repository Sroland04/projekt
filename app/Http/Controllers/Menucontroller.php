<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Menucontroller extends Controller
{
public function create(){
    return view('menu.create');
}

public function store(Request $request){

    $request->validate([
        'image' => 'required|mimes:jpg,png,jpeg|max:2048|unique:menus'
    ],[
        'image.required'=>'Kérjük válassza ki a feltölteni kívánt étlapot',
        'image.mimes'=>'A File csak png,jpg és jpeg formátumban tölthető fel',
        'image.max'=>'A file maximum 20 mb lehet',
        'image.unique'=>'Ez a Menü már fel lett töltve'
    ]);


    $data = new Menu();
    if ($request->file('image')){
        $file = $request->file('image');
        $filename= date('YmdHi').$file->getClientOriginalName();
        $file-> move(public_path('storage/app/public/img'), $filename);
        $data['image']= $filename;
    }

        $data->created_by = auth()->user()->id;
        $data->save();
        return back()->with('success', 'Az Étlap feltöltése sikeres volt');




}


public function index(){

    $menus = Menu::all();

    return view('menu.index',['menus'=>$menus]);
}


public function destroy($id){
$menu = Menu::find($id);
$menu->delete();
$menu->update();
return back()->with('success','Az étlap sikeresen törölve lett');
}

public function upload($id){

    Menu::find($id);
    DB::update("UPDATE `menus` SET `current`= 0 WHERE menus.current = 1 ");
    DB::update("UPDATE `menus` SET `current`= 1 WHERE menus.id = '$id'");
    return back()->with('success','Az étlap kiválasztva heti étlapként!');
}
}
