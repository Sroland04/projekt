<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $input = $request->all();
        $this->validate($request,[
            'email'=>'required|email',
            'password' => 'required',
        ],[
            'email.required'=>'Kérjük add meg az email címet',
            'email.email'=>'Kérjük figyelj az email formátumára',
            'password.required'=>'Kérjük add meg a jelszavad'

        ]);
        if (auth()->attempt(array('email'=>$input['email'],'password'=>$input['password'])))
        {
            if (auth()->user()->is_admin=="Admin"){
                return redirect()->route('adminhome');
            }else{
                return redirect()->route('parenthome');
            }
        }else{
          return back()->with('error','Az email-cím vagy jelszó helytelen');
        }
    }
}
