<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::parent;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone'=>['required','numeric','digits:9','unique:users'],
            'birth' => ['required', 'date'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ],
            [
                'name.required'=>'Kérjük add meg a neved',
                'name.regex'=>'A nevedben csak betük szerepelhetnek',
                'email.required'=>'Kérjük add meg az email-címed',
                'email.email'=>'Kérjük ügyelj az email formátumára',
                'email.unique'=>'Ez az email-cím már létezik',
                'phone.required'=>'Kérjük add meg a telefonszámod',
                'phone.numeric'=>'A telefonszámodban csak számok szerepelhetnek',
                'phone.digits'=>'A telefonszámnak 9 számból kell állnia',
                'phone.unique'=>'Ez a telefonszám már létezik',
                'birth.required'=>'Kérjük add meg a születési dátumodat',
                'birth.date'=>'A születési ddátum csak dátum lehet',
                'password.required'=>'Kérjük add meg a jelszavadat',
                'password.min'=>'A jelszónak minimum 8 karakterből kell állnia',
                'password.confirmed'=>'A jelszavak nem egyeznek'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone'=>$data['phone'],
            'birth'=>$data['birth'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
