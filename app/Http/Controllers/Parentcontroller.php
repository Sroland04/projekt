<?php

namespace App\Http\Controllers;

use App\Models\Child;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Parentcontroller extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index (){
        return view('parent.index');
    }

    public function profil(){
        return view('parent.profil');
    }

    public function payshow(){

        $id=Auth::id();

        $children = DB::select("SELECT children.name FROM children WHERE children.parentId = '$id'");

        return view('parent.payshow',['children'=>$children]);
    }

    public function pay(Request $request){

            $request->validate([
                'name'=>'required',
                'until'=>'required'
            ],[
                'name.required'=>'Kérjük adja meg gyermeke nevét',
                'until.required'=>'Kérjük Adja meg a Befizetés végét'
            ]);


        $name = $request->get('name');
        $until = $request->get('until');



        DB::update("UPDATE `children` SET `payed`= 1,`until`= '$until' WHERE children.name = '$name'");


        return back()->with('success','Az ebéd befizetése sikeres volt');
        }






        public function cancelshow(Request $request){

            $id=Auth::id();
            $children = DB::select("SELECT children.name,children.until FROM children WHERE children.parentId = '$id'");

        return view('parent.cancelshow',['children'=>$children]);

        }


        public function cancel(Request $request){

            $name = $request->input('name');

                DB::update("UPDATE `children` SET `payed`= 0,`until`= null WHERE children.name = '$name'");

                return back()->with('success','Az étkezés lemondása sikeresen megtörtént');
        }


        public function show(){
            $id=Auth::id();
        $children=DB::select("SELECT children.name,children.until,children.birth,children.age,children.Omnumber FROM children WHERE children.parentId = '$id'");

            return view('parent.show',['children'=>$children]);
        }


        public function menushow(){

        $data = DB::select('SELECT menus.image FROM menus
WHERE menus.current = 1;');
        return view('parent.menushow',['data'=>$data]);
        }



}
