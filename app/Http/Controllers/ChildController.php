<?php

namespace App\Http\Controllers;

use App\Models\Child;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\New_;

class ChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::select('SELECT children.id,children.name,children.birth,children.age,children.Omnumber,children.payed,children.until,users.name as szulonev
FROM children
INNER JOIN users ON users.id = children.parentId;');
        return view('child.index',["data"=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('child.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        $validate=\Illuminate\Support\Facades\Validator::make($request->all(),[
           'name'=>'required|regex:/^[\pL\s\-]+$/u|max:255',
            'birth'=>'required|date',
            'age'=>'required|integer|digits:2',
            'Omnumber'=>'required|integer|digits:9',
            'parentId'=>'required|integer'
        ],[
            'name.required'=>'Kérjük add meg a gyerek nevét',
            'name.regex'=>'A névben csak betük szerepelhetnek',
            'name.max'=>'A név maximum 255 karakter lehet',
            'birth.required'=>'Kérjük add meg a gyerek születési dátumát',
            'birth.date'=>'Figyelj a dátum formátumára',
            'age.required'=>'Kérjük add meg a gyerek életkorát',
            'age.integer'=>'Az életkorban csak betük szerpelhetnek',
            'age.digits'=>'Az életkkor maximum 2 karakter lehet',
            'Omnumber.required'=>'Kérjük add meg a gyerek étkezési azonosítóját',
            'Omnumber.integer'=>'Az étkezési azonosító csak számokból állhat',
            'Omnumber.digits'=>'Az étkezési azonosító csak 9 karakterből állhat',
            'parentId.required'=>'Kérjük adja meg a gyerekhez tartozó szülő Id-jét',
            'parentId.integer'=>'A szülő Id-je csak számokból állhat',
        ]);

                $data = new Child([
                    'id'=>$request->get('id'),
                    'name'=>$request->get('name'),
                    'birth'=>$request->get('birth'),
                    'age'=>$request->get('age'),
                    'Omnumber'=>$request->get('Omnumber'),
                    'parentId'=>$request->get('parentId')
                ]);


        if($validate->fails()) {
            return redirect('child.index')->withErrors($validate->errors())->withInput();
        }
        else{
            $data->save();
            return redirect('child.index',)->with('success','A gyerek hozzáadása sikeres volt');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Child  $child
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Child $child,Request $request)
    {

        $request->request->remove('_token');
        $request->request->remove('submit');


        return view('child.update',['child'=>$request]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Child  $child
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Child $child)
    {
      $children = Child::all();
      return view('child.edit',['children'=>$children]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Child  $child
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Child $child)
    {

        $validate=\Illuminate\Support\Facades\Validator::make($request->all(),[
            'name'=>'required|regex:/^[\pL\s\-]+$/u|max:255',
            'birth'=>'required|date',
            'age'=>'required|integer|max:2',
            'Omnumber'=>'required|integer|digits:9'
        ],[
            'name.required'=>'Kérjük add meg a gyerek nevét',
            'name.regex'=>'A névben csak betük szerepelhetnek',
            'name.max'=>'A név maximum 255 karakter lehet',
            'birth.required'=>'Kérjük add meg a gyerek születési dátumát',
            'birth.date'=>'Figyelj a dátum formátumára',
            'age.required'=>'Kérjük add meg a gyerek életkorát',
            'age.integer'=>'Az életkorban csak betük szerpelhetnek',
            'age.digits'=>'Az életkkor maximum 2 karakter lehet',
            'Omnumber.required'=>'Kérjük add meg a gyerek étkezési azonosítóját',
            'Omnumber.integer'=>'Az étkezési azonosító csak számokból állhat',
            'Omnumber.digits'=>'Az étkezési azonosító csak 9 karakterből állhat'
        ]);

        $child = Child::find($request->id);
        $child->name = $request->name;
        $child->birth = $request->birth;
        $child->age = $request->age;
        $child->Omnumber = $request->Omnumber;


        if($validate->fails()) {
            return redirect('child.index')->withErrors($validate->errors())->withInput();
        }
        else{
            $child->update();
            return redirect('child.index',)->with('success','Az adatok frissitése sikeres volt');
        }






    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Child  $child
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function destroy(Child $child)
    {
        $children=Child::all();
        return view('child.delete',['children'=>$children]);
    }

    public function delete($id){
        $child = Child::find($id);
        $child->delete();
        $child->update();
        return back()->with('success','A Gyerek törlése sikeresen megtörtént');

    }
}
