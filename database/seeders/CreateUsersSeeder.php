<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
            'name' => 'Admin',
            'email' => 'admin@onlinewebtutorblog.com',
            'is_admin' => 'Admin',
                'phone'=>'12345678',
                'birth'=>'1985-01-22',
            'password' => bcrypt('123456'),
        ],
            [
                'name' => 'User',
                'email' => 'normal@onlinewebtutorblog.com',
                'is_admin' => 'Szülő',
                'phone'=>'7377442',
                'birth'=>'1999-12-01',
                'password' => bcrypt('123456'),
                ],
            ];
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
