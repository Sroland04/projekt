<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Child>
 */
class ChildFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name'=>fake()->name(),
            'birth'=>fake()->dateTimeBetween('2008-01-01','2017-12-31'),
            'age'=>fake()->numberBetween('6','15'),
            'Omnumber'=>fake()->randomNumber('9'),
            'parentId'=>fake()->numberBetween('1','50')
        ];
    }
}
