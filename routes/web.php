<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
#region index_routes
Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
#endregion

#region admin_routes
Route::get('/admin.index',[\App\Http\Controllers\AdminController::class,'index'])->name('adminhome')->middleware('is_admin');
Route::get('/parent.index',[\App\Http\Controllers\Parentcontroller::class,'index'])->name('parenthome');
Route::get('/admin.update',[\App\Http\Controllers\AdminController::class,'edit'])->name('adminup');
Route::post('/admin.show',[\App\Http\Controllers\AdminController::class,'show'])->name('adminshow');
Route::post('/admin.edit',[\App\Http\Controllers\AdminController::class,'update'])->name('admined');
Route::get('/admin.delete',[\App\Http\Controllers\AdminController::class,'delete'])->name('admindelete');
Route::delete('/user/{id}',[\App\Http\Controllers\AdminController::class,'destroy'])->name('admindestroy');
#endregion_routes




#region Child-routes
Route::get('/child.index',[\App\Http\Controllers\ChildController::class,'index'])->name('childindex');
Route::get('/child.create',[\App\Http\Controllers\ChildController::class,'create'])->name('childcreate');
Route::post('/child.store',[\App\Http\Controllers\ChildController::class,'store'])->name('childstore');
Route::get('/child.edit',[\App\Http\Controllers\ChildController::class,'edit'])->name('childedit');
Route::post('/child.show',[\App\Http\Controllers\ChildController::class,'show'])->name('childshow');
Route::post('/child.update',[\App\Http\Controllers\ChildController::class,'update'])->name('childup');
Route::delete('/child/{id}',[\App\Http\Controllers\ChildController::class,'delete'])->name('childelete');
Route::get('/child.destroy',[\App\Http\Controllers\ChildController::class,'destroy'])->name('childestroy');
#endregion


#region Menu-routes
Route::get('/menu.create',[\App\Http\Controllers\Menucontroller::class,'create'])->name('menucreate');
Route::post('/menu.store',[\App\Http\Controllers\Menucontroller::class,'store'])->name('menustore');
Route::get('/menu.index',[\App\Http\Controllers\Menucontroller::class,'index'])->name('menuindex');
Route::delete('/menu/{id}',[\App\Http\Controllers\Menucontroller::class,'destroy'])->name('menudestroy');
Route::post('/menu/{id}',[\App\Http\Controllers\Menucontroller::class,'upload'])->name('menupload');
#endregion



#region Parent-routes
Route::get('/parent.profil',[\App\Http\Controllers\Parentcontroller::class,'profil'])->name('parentprofil');
Route::get('/parent.payshow',[\App\Http\Controllers\Parentcontroller::class,'payshow'])->name('parentpayshow');
Route::post('/parent.pay',[\App\Http\Controllers\Parentcontroller::class,'pay'])->name('parentpay');
Route::get('/parent.cancelshow',[\App\Http\Controllers\Parentcontroller::class,'cancelshow'])->name('cancel');
Route::post('/parent.cancel',[\App\Http\Controllers\Parentcontroller::class,'cancel'])->name('parentcancel');
Route::get('/parent.show',[\App\Http\Controllers\Parentcontroller::class,'show'])->name('parentshow');
Route::get('/parent.menushow',[\App\Http\Controllers\Parentcontroller::class,'menushow'])->name('parentmenushow');



#endregion



